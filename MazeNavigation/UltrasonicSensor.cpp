/*
  UltrasonicSensor.cpp - Library for HC-SR04 Ultrasonic Ranging Module.library
  Created by ITead studio. Apr 20, 2010.
  iteadstudio.com

  modified by IGOR CHAME (igorchame@poli.ufrj.br)
  on 04-Dec-2014
*/

#include "UltrasonicSensor.h"


/**
 * \brief Constructor of the class. Receives two integers that represents the trigger and echo pins of 
          the ultrassonic sensor. 
 *
 * requires: TP !=null && EP != null
 * modifies: this
 * effects: constructor.
 */
UltrasonicSensor::UltrasonicSensor(int TP, int EP)
{
    pinMode(TP,OUTPUT);
    pinMode(EP,INPUT);
    Trig_pin=TP;
    Echo_pin=EP;
    Samples_Quantity = SAMPLES_QUANTITY_DEFAULT;

    MEASURY_UNIT_CM = CM;
    MEASURY_UNIT_INC = INC;
    DELAY_BETWEEN_TRIGGERS = DELAY_BETWEEN_TRIGGERS_DEFAULT;
    timeLastTrigger = 0;
}

/**
 * \brief Second constructor of the class. Receives two integers that represents the trigger and echo pins of 
          the ultrassonic sensor. Besides that, receives another integer representing the number of samples
 *
 * requires: TP !=null && EP != null && samplesQnt != null
 * modifies: this
 * effects: constructor.
 */
UltrasonicSensor::UltrasonicSensor(int TP, int EP, int samplesQnt)
{
    pinMode(TP,OUTPUT);
    pinMode(EP,INPUT);
    Trig_pin=TP;
    Echo_pin=EP;
    Samples_Quantity = samplesQnt;

    MEASURY_UNIT_CM = CM;
    MEASURY_UNIT_INC = INC;
    DELAY_BETWEEN_TRIGGERS = DELAY_BETWEEN_TRIGGERS_DEFAULT;
}

/**
 * \brief This method returns the time the echo takes to return to the sensor
 *
 * modifies: this
 * effects: returns duration = timeReceived - timeEmited.
 */
long UltrasonicSensor::Timing()
{
    /* Ensuring that the estimation period is of DELAY_BETWEEN_TRIGGERS minimum
     */
    waitBeforeNextMeasure = timeLastTrigger + DELAY_BETWEEN_TRIGGERS - millis();
    if (waitBeforeNextMeasure > 0) { delay(waitBeforeNextMeasure); }
    timeLastTrigger = millis();

    digitalWrite(Trig_pin, LOW);
    delayMicroseconds(2);
    digitalWrite(Trig_pin, HIGH);
    delayMicroseconds(10);
    digitalWrite(Trig_pin, LOW);
    duration = pulseIn(Echo_pin,HIGH,5000);
    return duration;
}

/**
 * \brief This method measure the distance from the sensor utilising the Timing() method
 *
 * modifies: this
 * effects: returns distanceFromObstacle.
 */
long UltrasonicSensor::MeasureDistance(int sys)
{
  Timing();

  if (!duration) return 5000/29/2;
  distance_cm = duration /29 / 2 ;
  distance_inc = duration / 74 / 2;
  delay(60);
  if (sys)
  return distance_cm;
  else
  return distance_inc;
}

/**
 * \brief This method measure the distance from the sensor utilising the Timing() method. However, different from
 *        the measureDistance it takes the average value in a bunch of measurements.
 *
 * modifies: this
 * effects: returns distanceFromObstacle = (distance0 + distance1 + distance2.... + distanceN)/N.
 */
long UltrasonicSensor::MeasureFilteredDistance(int sys)
{
    /* Filtering the ultrasonic distance measure by
     * creating an average of #'SAMPLES_QUANTITY' samples
     */
    distance_aux = 0;
    for (counter = 0;counter < Samples_Quantity; counter++)
    {
        distance_aux += MeasureDistance(sys);
    }
    return (distance_aux/Samples_Quantity);
}
