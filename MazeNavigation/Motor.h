/*
  Motor.cpp - Library for controlling a motor
  created by IGOR CHAME (igorchame@poli.ufrj.br)
  on Dec-2014
*/

#include "Arduino.h"

#ifndef Motor_h
#define Motor_h

class Motor
{
    public:
        Motor(const int MotorParametersArg[]);
        Motor();
        int SetSpeed(int SpeedPercentage);
        int GetSpeed();
        void Brake();
        int GetCurrent();

    private:
        int PWM_PIN;
        int DIR_PIN;
        int BRK_PIN;
        int SNS_PIN;
        int SPEED_PERCENTAGE;


};

#endif
