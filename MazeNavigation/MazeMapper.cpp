

#include "MazeMapper.h"


/**
 * \brief Constructor of the class. Creates the maze
 *
 * modifies: this
 * effects: constructor.
 */
MazeMapper::MazeMapper()
{

    NORTH = NORTH_DEFAULT;
    WEST = WEST_DEFAULT;
    EAST = EAST_DEFAULT;
    SOUTH = SOUTH_DEFAULT;
    IDLE = IDLE_DEFAULT;

 //initialize verticalWalls (add exterior walls)
    for(byte i = 0;i < ROWS;i++)
    {
        for(byte j = 0;j<COLUMNS+1;j++)
        {
            if(j==0 || j == COLUMNS)
            {
                verticalWalls[i][j] = true;
            }
        }
    }

    //initialize horizontalWalls (add exterior walls)
    for(byte i = 0;i < ROWS + 1;i++)
    {
        for(byte j=0;j<COLUMNS;j++)
        {
            if(i==0 || i==ROWS)
            {
                horizontalWalls[i][j]=true;
            }
        }
    }

}

/**
 * \brief Sets the position of the robot in the maze
 * \param row row of the maze
 * \param column column of the maze
 *
 * requires: row! = null && column! = null
 * modifies: this
 * effects: robotColumn = column && robotRow = row
 */
void MazeMapper::setRobotPosition(byte row, byte column)
{
robotColumn = column;
robotRow = row;
}


/**
 * \brief Gets the row of the robot in the maze
 *
 * effects: returns RobotRow
 */
byte MazeMapper::getRobotRow()
{
    byte c = robotRow;
    return c;
}

/**
 * \brief Applies a base transfomation to the heading. The argument's heading is based
 *  that the robot was facing north prior to the MazeGuider movement.
 *
 * requires: dir! == null
 * modifies: this
 * effects: returns robotHeading
 */
 byte MazeMapper::convertDirection(byte dir){
    switch(dir){
        case NORTH_DEFAULT:
        //robotHeading = robotHeading;
        break;
        case SOUTH_DEFAULT:
        switch(robotHeading){
            case NORTH_DEFAULT:
            robotHeading = SOUTH_DEFAULT;
            break;
            case SOUTH_DEFAULT:
            robotHeading = NORTH_DEFAULT;
            break;
            case EAST_DEFAULT:
            robotHeading = WEST_DEFAULT;
            break;
            case WEST_DEFAULT:
            robotHeading = EAST_DEFAULT;
            break;
        }
        break;
        case WEST_DEFAULT:
           switch(robotHeading){
            case NORTH_DEFAULT:
            robotHeading = WEST_DEFAULT;
            break;
            case SOUTH_DEFAULT:
            robotHeading = EAST_DEFAULT;
            break;
            case EAST_DEFAULT:
            robotHeading = NORTH_DEFAULT;
            break;
            case WEST_DEFAULT:
            robotHeading = SOUTH_DEFAULT;
            break;
        }
        break;
        case EAST_DEFAULT:
        switch(robotHeading){
            case NORTH_DEFAULT:
            robotHeading = EAST_DEFAULT;
            break;
            case SOUTH_DEFAULT:
            robotHeading = WEST_DEFAULT;
            break;
            case EAST_DEFAULT:
            robotHeading = SOUTH_DEFAULT;
            break;
            case WEST_DEFAULT:
            robotHeading = NORTH_DEFAULT;
            break;
        }
        break;
    }

    return robotHeading;
 }

/**
 * \brief Gets the column of the robot in the maze
 *
 * effects: returns RobotColumn
 */
byte MazeMapper::getRobotColumn()
{
    byte c = robotColumn;
    return c;
}

/**
 * \brief Gets the direction the robot is heading
 *
 * effects: returns robotHeading
 */
byte MazeMapper::getRobotHeading()
{
    byte c = robotHeading;
    return c;
}

/**
 * \brief Sets the direction the robot is heading
 *
 * requires: dir < 4
 * modifies: this
 * effects: robotHeading = dir
 */
void MazeMapper::setRobotDirection(byte dir)
{
    if(dir > 3){

    } else{
        robotHeading = dir;
    }
}


/**
 * \brief Set the target block position and set the distance of each block of the maze to it
 *
 * requires: -1 < row < ROWS && -1 < column < COLUMNS
 * modifies: this
 * effects: targetBlockROW = row && targetBlockCOLUMN = column && blockXRowDistance = blockXRow - row 
 *          && blockXColumnDistance = blockXColumn - column
 */
void MazeMapper::setTargetBlock(byte row, byte column)
{

byte count = 0;

    for(int i = row; i< ROWS; i++)
    {
        count = i - row;
        for(int j = column; j < COLUMNS; j++)
        {
            values[i][j] = count;
            count++;
            
        }
        count = i - row;
        for(int j = column; j >= 0; j--)
        {
            values[i][j] = count;
            count++;
            
        }
    
    }

    for(int i = row; i >= 0; i--)
    {
        count = row-i;
        for(int j = column; j < COLUMNS; j++)
        {
            values[i][j] = count;
            count++;
            
        }
        count = row-i;
        for(int j = column; j >= 0; j--)
        {
            values[i][j] = count;
            count++;
            
        }
    }



}


/**
 * \brief add walls to a cell depending of it state.
 *
 * requires: -1 < row < ROWS && -1 < column < COLUMNS && state! = null
 * modifies: this
 * effects: blockWalls[row][column] = state.walls
 */
void MazeMapper::updateWalls( int dir, byte state)
{
    if(dir != IDLE){
    robotHeading = dir;
    }


    switch(robotHeading){
        case NORTH_DEFAULT:
        if(dir != IDLE){
        robotRow = robotRow - 1;}
        if(state == 1 || state == 3 || state == 5 || state == 7){
            horizontalWalls[robotRow][robotColumn] = true;
        } else{
            horizontalWalls[robotRow][robotColumn] = false;
        }
        if(state == 2 || state == 3 || state == 6 ||state == 7){
            verticalWalls[robotRow][robotColumn] = true;
        } else{
            verticalWalls[robotRow][robotColumn] = false;
        }
        if(state == 4 || state == 6 || state == 5 || state == 7){
            verticalWalls[robotRow][robotColumn+1] = true;
        } else{
            verticalWalls[robotRow][robotColumn+1] = false;
        }
        break;
        case EAST_DEFAULT:
        if(dir != IDLE){
        robotColumn = robotColumn + 1;}
        if(state == 2 || state == 3 || state == 6 || state == 7){
            horizontalWalls[robotRow][robotColumn] = true;
        } else{
            horizontalWalls[robotRow][robotColumn] = false;
        }
        if(state == 1 || state == 3 || state == 5 || state == 7){
            verticalWalls[robotRow][robotColumn+1] = true;
        } else{
            verticalWalls[robotRow][robotColumn+1] = false;
        }
        if(state == 4 || state == 5 || state == 6 || state == 7){
        horizontalWalls[robotRow+1][robotColumn] = true;
        } else {
        horizontalWalls[robotRow+1][robotColumn] = false;
        }
        break;
        case WEST_DEFAULT:
        if(dir != IDLE){
        robotColumn = robotColumn - 1;}
        if(state == 4 || state == 5 || state == 6 || state == 7){
            horizontalWalls[robotRow][robotColumn] = true;
        } else{
            horizontalWalls[robotRow][robotColumn] = false;
        }
        if(state == 1 || state == 3 || state == 5 || state == 7){
            verticalWalls[robotRow][robotColumn] = true;
        } else{
            verticalWalls[robotRow][robotColumn] = false;
        }
        if(state == 2 || state == 3 || state == 6 || state == 7){
        horizontalWalls[robotRow+1][robotColumn] = true;
        } else {
        horizontalWalls[robotRow+1][robotColumn] = false;
        }
        break;
        case SOUTH_DEFAULT:
        if(dir != IDLE){
        robotRow = robotRow + 1;}
        if(state == 4 || state == 5 || state == 6 || state == 7){
            verticalWalls[robotRow][robotColumn] = true;
        } else{
            verticalWalls[robotRow][robotColumn] = false;
        }
        if(state == 2 || state == 3 || state == 6 || state == 7){
            verticalWalls[robotRow][robotColumn+1] = true;
        } else{
            verticalWalls[robotRow][robotColumn+1] = false;
        }
        if(state == 1 || state == 3 || state == 5 || state == 7){
            horizontalWalls[robotRow+1][robotColumn] = true;
        } else {
            horizontalWalls[robotRow+1][robotColumn] = false;
        }
        break;
    }
   
}

void MazeMapper::solveKnownMaze(MazeMapper m)
{

}

void MazeMapper::solveUnknownMaze()
{

}

/**
 * \brief Print the current maze.
 *
 */
void MazeMapper::print()
    {
        for(byte i = 0;i < 2*ROWS+1;i++)
        {
            for(byte j = 0;j < 2*COLUMNS+1;j++)
            {
                //Add Horizontal Walls
                if(i%2 == 0 && j%2 == 1)
                {
                    if(horizontalWalls[i/2][j/2] == true)
                    {
                        Serial.print(" ---");
                    }
                    else
                    {
                        Serial.print("    ");
                    }
                }

                //Add Vertical Walls
                if(i%2 == 1 && j%2 == 0)
                {
                    if(verticalWalls[i/2][j/2] == true)
                    {
                        Serial.print("|");
                    }
                    else
                    {
                        Serial.print(" ");
                    }
                }

                //Add Flood Fill Values
                if(i%2 == 1 && j%2== 1)
                {
                    if((i-1)/2 == robotRow && (j-1)/2 == robotColumn)
                    {
                        if(robotHeading == 0)
                        {
                            Serial.print(" ^ ");
                        }
                        else if(robotHeading == 1)
                        {
                            Serial.print(" > ");
                        }
                        else if(robotHeading == 2)
                        {
                            Serial.print(" v ");
                        }
                        else if(robotHeading == 3)
                        {
                            Serial.print(" < ");
                        }
                    }
                    else
                    {
                        byte value = values[(i-1)/2][(j-1)/2];
                        if(value >= 100)
                        {
                            //Serial.print(value);
                            Serial.print("   ");
                        }
                        else
                        {
                            Serial.print(" ");
                            //Serial.print(value);
                            Serial.print(" ");
                        }
                        if(value < 10)
                        {
                            Serial.print(" ");
                        }
                    }
                }
            }
            Serial.print("\n");
        }
        Serial.print("\n");
    }
