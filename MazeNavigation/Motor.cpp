/*
  Motor.cpp - Library for controlling a motor
  created by IGOR CHAME (igorchame@poli.ufrj.br)
  on Dec-2014
*/
#include "Motor.h"


/**
 * \brief Constructor of the class.
 *
 * requires: motorParametersArg[].length >= 4
 * modifies: this
 * effects: constructor, parameters given.
 */
Motor::Motor(const int MotorParametersArg[])
{
    // Setting Parameters
    PWM_PIN = MotorParametersArg[0];
    DIR_PIN = MotorParametersArg[1];
    BRK_PIN = MotorParametersArg[2];
    SNS_PIN = MotorParametersArg[3];

    // Configure the pins for the motors
    pinMode(BRK_PIN, OUTPUT);  // Brake pin on
    pinMode(DIR_PIN, OUTPUT);    // Direction pin

    Brake();
}
/**
 * \brief Constructor of the class
 *
 * modifies: this
 * effects: constructor, parameters not given
 */
Motor::Motor()
{
    // Setting Parameters
    PWM_PIN = 0;
    DIR_PIN = 0;
    BRK_PIN = 0;
    SNS_PIN = 0;
}

/**
 * \brief Sets the robot speed.
 *
 * requires: SpeedPercentage! = null
 * modifies: this
 * effects: speed = abs(SpeedPercentage). if(SpeedPercentage < 0){speed = -speed}
 * returns current
 */
int Motor::SetSpeed(int SpeedPercentage)
{
//    Serial.print("Motor (");
//    Serial.print(PWM_PIN);
//    Serial.print(", ");
//    Serial.print(DIR_PIN);
//    Serial.print(", ");
//    Serial.print(BRK_PIN);
//    Serial.print(", ");
//    Serial.print(SNS_PIN);
//    Serial.println(")");
//    Serial.println(SpeedPercentage);
    SPEED_PERCENTAGE = SpeedPercentage;

    if (SpeedPercentage > 0)
    {
        digitalWrite(BRK_PIN, LOW);  // setting brake LOW disable motor brake
        digitalWrite(DIR_PIN, LOW);   // setting direction to HIGH the motor will spin forward
    } else {
        digitalWrite(BRK_PIN, LOW);  // setting againg the brake LOW to disable motor brake
        digitalWrite(DIR_PIN, HIGH);    // now change the direction to backward setting LOW
    }

    SpeedPercentage = abs(SpeedPercentage);

    analogWrite(PWM_PIN, map(SpeedPercentage,0,100,0,255));

    // returns the current
    return analogRead(SNS_PIN);
}

/**
 * \brief Get the robot speed.
 */
int Motor::GetSpeed()
{
    return SPEED_PERCENTAGE;
}

/**
 * \brief Brake the robot, seting the break pin to HIGH
 */
void Motor::Brake()
{
    digitalWrite(BRK_PIN, HIGH);  // raise the brake
    return;
}

/**
 * \brief Get the current passed through the robot.
 *
 * effects: returns current.
 */
int Motor::GetCurrent()
{
    return analogRead(SNS_PIN);
}
