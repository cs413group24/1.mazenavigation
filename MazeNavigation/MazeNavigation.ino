#include "Header.h"
#include "UltrasonicSensor.h"
#include "Steer.h"
#include "Motor.h"
#include "MazeGuider.h"
#include "MazeMapper.h"

UltrasonicSensor SensorFrontUnit(DIST_TRIG_FRONT,DIST_ECHO_FRONT);
UltrasonicSensor SensorRightUnit(DIST_TRIG_RIGHT,DIST_ECHO_RIGHT);
UltrasonicSensor SensorLeftUnit(DIST_TRIG_LEFT,DIST_ECHO_LEFT);

Motor MotorRightUnit(motorParametersRight);
Motor MotorLeftUnit(motorParametersLeft);

Steer SteeringUnit(MotorRightUnit,MotorLeftUnit, SensorFrontUnit, SensorLeftUnit, SensorRightUnit);

MazeGuider MazeGuiderUnit(SteeringUnit, SensorFrontUnit, SensorLeftUnit, SensorRightUnit);

MazeMapper MazeMapperUnit = MazeMapper();

int incomingByte;
int usingSerialStepByStep = 1;

void setup() {

    randomSeed(analogRead(0));

    // Initializing Serial communication
    Serial.begin(9600);

    // Initializing message
    Serial.println("STARTING ARDUINO");
    delay(2000);
    Serial.println("Please Calibrate sensors before execution.");
    Serial.println("To start the calibration process please send \'S\' ");
    Serial.println("Please calibrate in a cell with walls on both sides. ");
    if (usingSerialStepByStep)
    {
        while(incomingByte != 'S'){incomingByte = Serial.read();}
    }
    MazeGuiderUnit.calibrate();
    delay(2000);

    MazeMapperUnit.setRobotPosition( 0, 0);
    MazeMapperUnit.setRobotDirection( 2);
    MazeMapperUnit.setTargetBlock( 0, 1);
    MazeMapperUnit.updateWalls(MazeMapperUnit.IDLE, MazeGuiderUnit.getState());
}

void loop() {
    if (usingSerialStepByStep)
    {
        if (Serial.available() > 0) {
            // read the oldest byte in the serial buffer:
            incomingByte = Serial.read();
            // if it's a capital S, execute:
            if (incomingByte == 'S') {
               // MazeGuiderUnit.executeStateDecision(MazeGuiderUnit.getState());
               SteeringUnit.TurnOwnAxis90Degrees(SteeringUnit.TURN_DIR_RIGHT);
            } else if (incomingByte == 'D') {
                SteeringUnit.TurnOwnAxis(90,SteeringUnit.TURN_DIR_RIGHT);
            } else if (incomingByte == 'M') {
               long f = SensorFrontUnit.MeasureFilteredDistance(SensorFrontUnit.MEASURY_UNIT_CM);
               long r = SensorRightUnit.MeasureFilteredDistance(SensorRightUnit.MEASURY_UNIT_CM);
               long l = SensorLeftUnit.MeasureFilteredDistance(SensorLeftUnit.MEASURY_UNIT_CM);
               Serial.print("f");
               Serial.print(f);
               Serial.print(" r");
               Serial.print(r);
               Serial.print(" l");
               Serial.println(l);
            } else if(incomingByte == 'F'){
                Serial.println();
                Serial.println();
            } else if (incomingByte == 'R') {
                MazeGuiderUnit.executeStateDecision(MazeGuiderUnit.getState());
            } else if(incomingByte == 'U'){
                byte dir = MazeGuiderUnit.executeStateDecision(6);
                byte newDir = MazeMapperUnit.convertDirection(dir);
                MazeMapperUnit.updateWalls(newDir, MazeGuiderUnit.getState());
                MazeMapperUnit.print();
                delay(1000);
            } else if(incomingByte == 'T'){
                MazeMapperUnit.updateWalls(MazeMapperUnit.IDLE, 6);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.SOUTH, 5);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.EAST, 3);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.SOUTH, 1);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.WEST, 7);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.EAST, 4);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.EAST, 5);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.NORTH, 6);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.NORTH, 5);
                MazeMapperUnit.print();
                MazeMapperUnit.updateWalls(MazeMapperUnit.WEST, 3);
                MazeMapperUnit.print();

            } else if(incomingByte == 'G'){
             byte dir = MazeGuiderUnit.executeStateDecision(MazeGuiderUnit.getState());
             byte newDir = MazeMapperUnit.convertDirection(dir);
             MazeMapperUnit.updateWalls(newDir, MazeGuiderUnit.getState());
             MazeMapperUnit.print();
             delay(1000);
            }
        }
    } else {
        MazeGuiderUnit.executeStateDecision(MazeGuiderUnit.getState());
       
        delay(1000);
    }


}




