/*
  MazeGuiderDecisionNode.cpp - Library for guiding a robot through a maze
  created by IGOR CHAME (igorchame@poli.ufrj.br)
  on Dec-2014
*/

#include "MazeGuiderDecisionNode.h"

/**
 * \brief Constructor of the class.
 *
 * Will construct a decision node based on a given state and a pointer to the father.
 * The pointer to the father could be NULL if there is not one.
 *
 * modifies: this
 * effects: constructor.
 */
MazeGuiderDecisionNode::MazeGuiderDecisionNode(MazeGuiderDecisionNode* parentDecisionNodeArg, int nodeStateArg) : parentDecisionNode(parentDecisionNodeArg) {
    _nodeState = (nodeStateArg);
    parentDecisionNode = parentDecisionNodeArg;
    alternativeNodeLeft = NULL;
    alternativeNodeRight = NULL;
    alternativeNodeFront = NULL;
    _LeftDeadEnd = 0;
    _RightDeadEnd = 0;
    _FrontDeadEnd = 0;
}

/**
 * @brief Destructor of the object.
 *
 * effect: delete the node itself and its sons.
 */
MazeGuiderDecisionNode::~MazeGuiderDecisionNode() {
    delete alternativeNodeLeft;
    delete alternativeNodeRight;
    delete alternativeNodeFront;
}

/**
 * @brief Get the State of the node.
 *
 * The state of the node is given by MazeGuider class. Check MazeGuider::getState().
 */
int MazeGuiderDecisionNode::getData() const {
    return _nodeState;
}

/**
 * @brief Set the state of the node.
 *
 * The state of the node is given by MazeGuider class. Check MazeGuider::getState().
 */
void MazeGuiderDecisionNode::setData(int nodeStateArg) {
    _nodeState = nodeStateArg;
}


MazeGuiderDecisionNode* MazeGuiderDecisionNode::addChild(int nodeStateArg, int alternativeNode) {
    switch (alternativeNode) {
        case ALTERNATIVE_NODE_FRONT:
            return alternativeNodeFront = new MazeGuiderDecisionNode(this, _nodeState);
            break;
        case ALTERNATIVE_NODE_LEFT:
            return alternativeNodeLeft = new MazeGuiderDecisionNode(this, _nodeState);
            break;
        case ALTERNATIVE_NODE_RIGHT:
            return alternativeNodeRight = new MazeGuiderDecisionNode(this, _nodeState);
            break;
        default:
            break;
    }
}

/**
 * \brief   Checks if one of the sons of this node has a
 *          pointer that is equal to the argument childToFind.
 *
 * requires: childToFind! = null
 * effects:  if(childToFind == someChild) returns someChild
 */
int MazeGuiderDecisionNode::findChild (MazeGuiderDecisionNode* childToFind) {
    if (childToFind == alternativeNodeFront)
        return ALTERNATIVE_NODE_FRONT;
    else if (childToFind == alternativeNodeLeft)
        return ALTERNATIVE_NODE_LEFT;
    else if (childToFind == alternativeNodeRight)
        return ALTERNATIVE_NODE_RIGHT;
    else
        return 0;
}

/**
 * \brief   Removes a child by deleting it.
 *
 * AlternativeNode can be ALTERNATIVE_NODE_FRONT or ALTERNATIVE_NODE_LEFT or ALTERNATIVE_NODE_RIGHT.
 *
 */
void MazeGuiderDecisionNode::removeChild(int alternativeNode) {
    switch (alternativeNode) {
        case ALTERNATIVE_NODE_FRONT:
            delete alternativeNodeFront;
            break;
        case ALTERNATIVE_NODE_LEFT:
            delete alternativeNodeLeft;
            break;
        case ALTERNATIVE_NODE_RIGHT:
            delete alternativeNodeRight;
            break;
        default:
            break;
    }
}

/**
 * \brief   Set a flag indicating that this child's path is a deadend.
 *
 * AlternativeNode can be ALTERNATIVE_NODE_FRONT or ALTERNATIVE_NODE_LEFT or ALTERNATIVE_NODE_RIGHT.
 *
 */
void MazeGuiderDecisionNode::setDeadEnd(int alternativeNode) {
    switch (alternativeNode) {
        case ALTERNATIVE_NODE_FRONT:
            _FrontDeadEnd = 1;
            break;
        case ALTERNATIVE_NODE_LEFT:
            _LeftDeadEnd = 1;
            break;
        case ALTERNATIVE_NODE_RIGHT:
            _RightDeadEnd = 1;
            break;
        default:
            break;
    }
}

/**
 * \brief   Get if the child's path is set as a deadend.
 *
 * AlternativeNode can be ALTERNATIVE_NODE_FRONT or ALTERNATIVE_NODE_LEFT or ALTERNATIVE_NODE_RIGHT.
 */
byte MazeGuiderDecisionNode::getDeadEnd(int alternativeNode) {
    switch (alternativeNode) {
        case ALTERNATIVE_NODE_FRONT:
            return _FrontDeadEnd;
            break;
        case ALTERNATIVE_NODE_LEFT:
            return _LeftDeadEnd;
            break;
        case ALTERNATIVE_NODE_RIGHT:
            return _RightDeadEnd;
            break;
        default:
            return 0;
            break;
    }
}

/**
 * \brief   Gets a pointer to the child selected.
 *
 * AlternativeNode can be ALTERNATIVE_NODE_FRONT or ALTERNATIVE_NODE_LEFT or ALTERNATIVE_NODE_RIGHT.
 *
 */
MazeGuiderDecisionNode* MazeGuiderDecisionNode::getChild(int alternativeNode) const {
    switch (alternativeNode) {
        case ALTERNATIVE_NODE_FRONT:
            return alternativeNodeFront;
            break;
        case ALTERNATIVE_NODE_LEFT:
            return alternativeNodeLeft;
            break;
        case ALTERNATIVE_NODE_RIGHT:
            return alternativeNodeRight;
            break;
        default:
            break;
    }
}

/**
 * \brief   Gets a pointer to the parend of the node.
 *
 */
MazeGuiderDecisionNode* MazeGuiderDecisionNode::getParent() const {
    return parentDecisionNode;
}
