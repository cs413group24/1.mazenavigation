/*
  UltrasonicSensor.h - Library for HR-SC04 Ultrasonic Ranging Module.
  Created by ITead studio. Alex, Apr 20, 2010.
  iteadstudio.com

  modified by IGOR CHAME (igorchame@poli.ufrj.br)
  on 04-Dec-2014
*/

#include "Arduino.h"

#ifndef UltrasonicSensor_h
#define UltrasonicSensor_h


#define CM 1
#define INC 0

#define DELAY_BETWEEN_TRIGGERS_DEFAULT 60
#define SAMPLES_QUANTITY_DEFAULT 2

class UltrasonicSensor
{
  public:
    UltrasonicSensor(int TP, int EP);
    UltrasonicSensor(int TP, int EP, int samplesQnt);
    long Timing();
    long MeasureDistance(int sys);
    long MeasureFilteredDistance (int sys);
    int MEASURY_UNIT_CM;
    int MEASURY_UNIT_INC;

    private:
    int DELAY_BETWEEN_TRIGGERS;
    int Trig_pin;
    int Echo_pin;
    int Samples_Quantity;
    long  duration,distance_cm,distance_inc,distance_aux;
    int counter;
    unsigned long timeLastTrigger;
    long waitBeforeNextMeasure;

};

#endif
