
/*
  MazeGuider.cpp - Library for guiding a robot through a maze
  created by IGOR CHAME (igorchame@poli.ufrj.br)
  on Dec-2014
*/

#include "Arduino.h"
#include "UltrasonicSensor.h"
#include "Steer.h"
#include "MazeGuiderDecisionNode.h"

#ifndef MazeGuider_h
#define MazeGuider_h

#define MAZE_BLOCK_SIZE_DEFAULT 25
#define TIME_TRAVEL_BETWEEN_BLOCKS_DEFAULT 5400
#define CALIBRATED_DISTANCE_WALL_RIGHT_DEFAULT 6
#define CALIBRATED_DISTANCE_WALL_LEFT_DEFAULT 6

class MazeGuider
{
    public:
        MazeGuider(Steer& argSteeringUnit, UltrasonicSensor& ArgSensorFront, UltrasonicSensor& ArgSensorLeft, UltrasonicSensor& ArgSensorRight);
        void driveToNextBlock();
        int getState();
        void calibrate();
        int isThereDecisionNode();
        byte executeStateDecision(int currentState);


    private:
        void returnPreviousDecision();
        int isThereObstacleAhead();
        int isTherePathToFront();
        int isTherePathToLeft();
        int isTherePathToRight();
        void calibrateDistanceToWalls();
        MazeGuiderDecisionNode* FirstDecisionNode;
        MazeGuiderDecisionNode* LastDecisionNode;
        Steer& _SteeringUnit; // reference to Steering Unit
        UltrasonicSensor& _SensorFrontUnit; // reference to Ultrassonic Sensor
        UltrasonicSensor& _SensorRightUnit; // reference to Ultrassonic Sensor
        UltrasonicSensor& _SensorLeftUnit; // reference to Ultrassonic Sensor
        long SensorFrontUnit_distanceEstimation;
        long SensorRightUnit_distanceEstimation;
        long SensorLeftUnit_distanceEstimation;
        int stateValue;
        int MAZE_BLOCK_SIZE;
        int TIME_TRAVEL_BETWEEN_BLOCKS;
        int CALIBRATED_DISTANCE_WALL_LEFT;
        int CALIBRATED_DISTANCE_WALL_RIGHT;
        unsigned long startingTime;
        byte stateReturn;
};

#endif
