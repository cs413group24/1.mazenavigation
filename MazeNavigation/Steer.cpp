/*
  Steer.cpp - Library for controlling two motors rover
  created by IGOR CHAME (igorchame@poli.ufrj.br)
  on 04-Dec-2014
*/
#include "Steer.h"

Motor MotorTemplateRight = Motor();
Motor MotorTemplateLeft = Motor();

Motor& Steer::_MotorRight = MotorTemplateRight;
Motor& Steer::_MotorLeft = MotorTemplateLeft;

UltrasonicSensor SensorModel = UltrasonicSensor(0,0);
UltrasonicSensor SensorModel2 = UltrasonicSensor(0,0);
UltrasonicSensor SensorModel3 = UltrasonicSensor(0,0);

UltrasonicSensor& Steer::_SensorFrontUnit = SensorModel;
UltrasonicSensor& Steer::_SensorLeftUnit = SensorModel2;
UltrasonicSensor& Steer::_SensorRightUnit = SensorModel3;
UltrasonicSensor& Steer::_RefiningSensor = SensorModel3;

/**
 * \brief Constructor of the class. Receives two objects of the class motor. 
 *
 * requires: (Motor) ML! = null && (Motor) MR != null && (UltrasonicSensor) ArgSensorFront! = (UltrasonicSensor) ArgSensorRight! = (UltrasonicSensor) ArgSensorLeft! = null
 * modifies: this
 * effects: constructor.
 */
Steer::Steer(Motor& ML, Motor& MR, UltrasonicSensor& ArgSensorFront, UltrasonicSensor& ArgSensorLeft, UltrasonicSensor& ArgSensorRight)
{
    _MotorRight = MR;
    _MotorLeft = ML;
    _SensorFrontUnit = ArgSensorFront;
    _SensorLeftUnit = ArgSensorLeft;
    _SensorRightUnit = ArgSensorRight;

    TURN_DIR_LEFT = TURN_DIR_LEFT_DEFAULT;
    TURN_DIR_RIGHT = TURN_DIR_RIGHT_DEFAULT;
    MS_TIME_FULL_TURN_OWN_AXIS = MS_TIME_FULL_TURN_OWN_AXIS_DEFAULT;
    TURNING_SPEED = TURNING_SPEED_DEFAULT;
    STRAIGHT_DRIVE_BALANCING_RIGHTMOTOR = STRAIGHT_DRIVE_BALANCING_RIGHTMOTOR_DEFAULT;
    STRAIGHT_DRIVE_BALANCING_LEFTMOTOR = STRAIGHT_DRIVE_BALANCING_LEFTMOTOR_DEFAULT;
    Kp = KP_DEFAULT;
    STANDARD_DRIVE_SPEED = STANDARD_DRIVE_SPEED_DEFAULT;
}

/**
 * \brief Set both motors to the same speed
 *
 * requires: SpeedPercentage! = null
 * modifies: (Motor) this._MotorRight && (Motor) this._MotorLeft
 * effects: _MotorRight.speed = _MotorLeft.speed = SpeedPercentage.
 */
void Steer::DriveStraight(int SpeedPercentage)
{
    _MotorRight.SetSpeed(SpeedPercentage+STRAIGHT_DRIVE_BALANCING_RIGHTMOTOR);
    _MotorLeft.SetSpeed(SpeedPercentage+STRAIGHT_DRIVE_BALANCING_LEFTMOTOR);
    return;
}

/**
 * \brief Tries to align the robot in the middle of the hallway, keeping it moving forward. 
 *        Therefore, if the robot is closer to the left wall, adjustedSpeedLeft < adjustedSpeedRight
 *
 * requires: SpeedPercentage! = null && Error! = null
 * modifies: (Motor) this._MotorRight && (Motor) this._MotorLeft
 * effects: MotorRightSpeed = adjustedSpeedRight && MotorLeftSpeed == adjustedSpeedLeft
 */
void Steer::DriveStraightProportional(int SpeedPercentage, int Error)
{
    Error = constrain (Error,-2,2);
    _MotorRight.SetSpeed(SpeedPercentage+STRAIGHT_DRIVE_BALANCING_RIGHTMOTOR-Kp*Error);
    _MotorLeft.SetSpeed(SpeedPercentage+STRAIGHT_DRIVE_BALANCING_LEFTMOTOR+Kp*Error);
    return;
}

/**
 * \brief Give an angle and direction, the robot will turn that angle around its own axis.
 *
 * requires: Angle! = null && 0 < turnDirection < 3
 * modifies: (Motor) this._MotorRight && (Motor) this._MotorLeft
 * effects: while(turning){ MotorRightSpeed = -MotorLeftSpeed}; break();
 */
void Steer::TurnOwnAxis(int Angle, const int TurnDirection)
{

    if (TurnDirection == TURN_DIR_LEFT)
    {
        if (_SensorLeftUnit.Timing() <  _SensorFrontUnit.Timing())
        {
            _RefiningSensor = _SensorFrontUnit;
        } else {
             _RefiningSensor = _SensorRightUnit;
        }
        _MotorLeft.SetSpeed(-TURNING_SPEED);
        _MotorRight.SetSpeed(+TURNING_SPEED);
    }
    else if (TurnDirection == TURN_DIR_RIGHT)
    {
        if (_SensorRightUnit.Timing() <  _SensorFrontUnit.Timing())
        {
            _RefiningSensor = _SensorFrontUnit;
        } else {
             _RefiningSensor = _SensorLeftUnit;
        }
        _MotorLeft.SetSpeed(+TURNING_SPEED);
        _MotorRight.SetSpeed(-TURNING_SPEED);
    }
    if (Angle == 180)
    {
        if (_SensorRightUnit.Timing() <  _SensorLeftUnit.Timing())
        {
            _RefiningSensor = _SensorLeftUnit;
        } else {
            _RefiningSensor = _SensorRightUnit;
        }
    }
    delay(GetDelayForTurn(Angle));

    /**
     * Refining the turn by using ultrasonic sensors.
     */

    newRefiningDistance = (_RefiningSensor.Timing()+_RefiningSensor.Timing())/2;
    aux = 0;
    do {
        refiningDistance = newRefiningDistance;
        delay (1);
        newRefiningDistance = (_RefiningSensor.Timing()+_RefiningSensor.Timing())/2;
        Serial.print("dist: ");
        Serial.println(newRefiningDistance);
        if(newRefiningDistance > refiningDistance){
            aux = aux + 2;
        } else {
            if(aux>0){
                aux--;
            }
        }
      } while (aux < 2);
    Brake();
    return;
}

/**
 * \brief Depending of a direction, makes the robot turns exactly 90 degrees around its axis, utilizing the ultrassonic sensors
 *
 * requires: 0 < TurnDirection < 3
 * effects: while(turn < 90degrees){motorRightSpeed = -motorLeftSpeed} break();
 */
void Steer::TurnOwnAxis90Degrees(const int TurnDirection)
{
    leftDistance = _SensorLeftUnit.MeasureFilteredDistance(_SensorLeftUnit.MEASURY_UNIT_CM);
    rightDistance = _SensorRightUnit.MeasureFilteredDistance(_SensorRightUnit.MEASURY_UNIT_CM);
    frontDistance = _SensorFrontUnit.MeasureFilteredDistance(_SensorFrontUnit.MEASURY_UNIT_CM);
    newLeftDistance = leftDistance;
    newRightDistance = rightDistance;
    newFrontDistance = newFrontDistance;
    
     if (TurnDirection == TURN_DIR_LEFT)
    {
        while(!(newFrontDistance == constrain(newFrontDistance, leftDistance - DIFERENCE_FRONT_SIDE_SENSOR - 1, leftDistance - DIFERENCE_FRONT_SIDE_SENSOR + 1)) 
            && !(newRightDistance == constrain(newRightDistance, frontDistance + DIFERENCE_FRONT_SIDE_SENSOR - 1 , frontDistance + DIFERENCE_FRONT_SIDE_SENSOR + 1)))
        {
            _MotorLeft.SetSpeed(-TURNING_SPEED);
            _MotorRight.SetSpeed(+TURNING_SPEED);
            delay(100);
            newLeftDistance = _SensorLeftUnit.MeasureFilteredDistance(_SensorLeftUnit.MEASURY_UNIT_CM);
            newRightDistance = _SensorRightUnit.MeasureFilteredDistance(_SensorRightUnit.MEASURY_UNIT_CM);
            newFrontDistance = _SensorFrontUnit.MeasureFilteredDistance(_SensorFrontUnit.MEASURY_UNIT_CM);
        }
    }
    else if (TurnDirection == TURN_DIR_RIGHT)
    {
       long t = millis() + GetDelayForTurn(100);
         while((!(newFrontDistance == constrain(newFrontDistance, rightDistance - DIFERENCE_FRONT_SIDE_SENSOR, rightDistance - (DIFERENCE_FRONT_SIDE_SENSOR + 1)))
            && !(newLeftDistance == constrain(newLeftDistance, frontDistance + DIFERENCE_FRONT_SIDE_SENSOR, frontDistance + DIFERENCE_FRONT_SIDE_SENSOR + 1))) && millis() < t)
        {
            _MotorLeft.SetSpeed(+TURNING_SPEED);
            _MotorRight.SetSpeed(-TURNING_SPEED);
            delay(25);
            newLeftDistance = _SensorLeftUnit.MeasureFilteredDistance(_SensorLeftUnit.MEASURY_UNIT_CM);
            newRightDistance = _SensorRightUnit.MeasureFilteredDistance(_SensorRightUnit.MEASURY_UNIT_CM);
            newFrontDistance = _SensorFrontUnit.MeasureFilteredDistance(_SensorFrontUnit.MEASURY_UNIT_CM);
        }

        do {
            frontDistance = newFrontDistance;
            delay (25);
            newFrontDistance = _SensorFrontUnit.MeasureFilteredDistance(_SensorFrontUnit.MEASURY_UNIT_CM);
        } while (newFrontDistance <= frontDistance);

    }
    Brake();
    return;
}

/**
 * \brief Depending of an angle and some pre-stabilished constants, returns how long the robot should
 *        turn to achieve that angle  
 *
 * requires: Angle! = null
 * effects: returns timeForAngle = timeFor360*angleInDeegre/360;
 */
int Steer::GetDelayForTurn(int Angle)
{
    return round((map(Angle,0,360,0,100)/100.0)*MS_TIME_FULL_TURN_OWN_AXIS);
}

/**
 * \brief Breaks both motors immediately
  */
void Steer::Brake()
{
    _MotorLeft.Brake();
    _MotorRight.Brake();
    return;
}
