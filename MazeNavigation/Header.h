#pragma once
#include <Arduino.h>


#ifndef Header_h
#define Header_h

/**
 * Function headers
 */



/**
 * Constants
 */

const int SPEED = 25; // Power of the motors
const int DISTANCE_MOVEMENT = 25; // How many cm should the robot walk forward/backward (1 maze block size)
const int ERR = 3;
const int TURNING_DELAY = 3200;

/* Motors Constant
 */
const int
PWM_A   = 3,
DIR_A   = 12,
BRAKE_A = 9,
SNS_A   = A0,
PWM_B   = 11,
DIR_B   = 13,
BRAKE_B = 8,
SNS_B   = A1;

const int motorParametersLeft[] = {PWM_A,DIR_A,BRAKE_A,SNS_A};
const int motorParametersRight[] = {PWM_B,DIR_B,BRAKE_B,SNS_B};

/* Ultrasonic Sensors Constant
 */
const int
DIST_TRIG_FRONT = 30,
DIST_ECHO_FRONT = 31,
DIST_TRIG_RIGHT = 32,
DIST_ECHO_RIGHT = 33,
DIST_TRIG_LEFT = 34,
DIST_ECHO_LEFT = 35;

const int middleDistance = 4;

#endif
