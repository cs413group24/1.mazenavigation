/*
  MazeGuider.cpp - Library for guiding a robot through a maze
  created by IGOR CHAME (igorchame@poli.ufrj.br)
  on Dec-2014
*/

#include "MazeGuider.h"
#include "MazeMapper.h"

/**
 * \brief Constructor of the class. Receives one object of the Steer class and three objects of UltrasonicSensor class.
 *
 * requires: (Steer) argSteeringUnit! = null && (UltrasonicSensor) ArgSensorFront != null && (UltrasonicSensor) ArgSensorLeft != null && (UltrasonicSensor) ArgSensorRight != null
 * modifies: this
 * effects: constructor.
 */
MazeGuider::MazeGuider(Steer& argSteeringUnit, UltrasonicSensor& ArgSensorFront, UltrasonicSensor& ArgSensorLeft, UltrasonicSensor& ArgSensorRight): _SteeringUnit(argSteeringUnit), _SensorFrontUnit(ArgSensorFront), _SensorLeftUnit(ArgSensorLeft), _SensorRightUnit(ArgSensorRight) //initializing references
{
    _SensorFrontUnit = ArgSensorFront;
    _SensorLeftUnit = ArgSensorLeft;
    _SensorRightUnit = ArgSensorRight;

    _SteeringUnit = argSteeringUnit;

    MAZE_BLOCK_SIZE = MAZE_BLOCK_SIZE_DEFAULT;
    TIME_TRAVEL_BETWEEN_BLOCKS = TIME_TRAVEL_BETWEEN_BLOCKS_DEFAULT;
    CALIBRATED_DISTANCE_WALL_LEFT = CALIBRATED_DISTANCE_WALL_LEFT_DEFAULT;
    CALIBRATED_DISTANCE_WALL_RIGHT = CALIBRATED_DISTANCE_WALL_RIGHT_DEFAULT;
}

/**
 * \brief Calibration method for measuring the distance to walls.
 *
 * modifies: this
 * effects: CalibratedLeftDistance = CalibratedRightDistance = sideWallDistance
 */
void MazeGuider::calibrate() {
    FirstDecisionNode = new MazeGuiderDecisionNode( NULL , getState());
    LastDecisionNode = FirstDecisionNode;
    LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_FRONT);

    calibrateDistanceToWalls();
}

/**
 * \brief Calibration method. Measures the center of two walls.
 *
 * modifies: this
 * effects: CalibratedLeftDistance = CalibratedRightDistance = sideWallDistance
 */
void MazeGuider::calibrateDistanceToWalls() {
    CALIBRATED_DISTANCE_WALL_LEFT = _SensorLeftUnit.MeasureFilteredDistance(_SensorLeftUnit.MEASURY_UNIT_CM);
    CALIBRATED_DISTANCE_WALL_RIGHT = _SensorRightUnit.MeasureFilteredDistance(_SensorRightUnit.MEASURY_UNIT_CM);
    CALIBRATED_DISTANCE_WALL_LEFT = ((CALIBRATED_DISTANCE_WALL_LEFT+CALIBRATED_DISTANCE_WALL_RIGHT)/2);
    CALIBRATED_DISTANCE_WALL_RIGHT = CALIBRATED_DISTANCE_WALL_LEFT;

    Serial.println("MazeGuider.calibrate()");
    Serial.print("CALIBRATED_DISTANCE_WALL_RIGHT ");
    Serial.println(CALIBRATED_DISTANCE_WALL_RIGHT);
    Serial.print("CALIBRATED_DISTANCE_WALL_LEFT ");
    Serial.println(CALIBRATED_DISTANCE_WALL_LEFT);
}

/**
 * \brief GetState Method. Get the current state of the robot.
 *
 * If the robot's current cell has no walls neither to the front, right and left the state is 0 (zero).
 * However, if there is a wall to the right: add 4 (four) to the state value
 * if there is a wall to the left: add 2 (two) to the state value
 * if there is a wall to the front: add a 1 (one) to the state value
 */
int MazeGuider::getState() {
    stateValue = 0;
    stateValue = 1*(!isTherePathToFront()) + 2*(!isTherePathToLeft()) + 4*(!isTherePathToRight());
    return stateValue;
}

/**
 * \brief Drive to the next block in the maze.
 *
 * modifies: this
 * effects: Since the maze is divided in blocks, this method makes the robot
 * to drive to the next block in front of it.
 */
void MazeGuider::driveToNextBlock(){
    startingTime = millis();

    while (!isThereObstacleAhead() && ((millis() - startingTime) < TIME_TRAVEL_BETWEEN_BLOCKS))
    {
        SensorLeftUnit_distanceEstimation = _SensorLeftUnit.MeasureFilteredDistance(_SensorLeftUnit.MEASURY_UNIT_CM);
        SensorRightUnit_distanceEstimation = _SensorRightUnit.MeasureFilteredDistance(_SensorRightUnit.MEASURY_UNIT_CM);
        
        if (SensorLeftUnit_distanceEstimation < (15) && SensorRightUnit_distanceEstimation < (15))
            _SteeringUnit.DriveStraightProportional(_SteeringUnit.STANDARD_DRIVE_SPEED,(SensorRightUnit_distanceEstimation-SensorLeftUnit_distanceEstimation));
        else if (SensorLeftUnit_distanceEstimation < (15))
            _SteeringUnit.DriveStraightProportional(_SteeringUnit.STANDARD_DRIVE_SPEED,((CALIBRATED_DISTANCE_WALL_RIGHT)-SensorLeftUnit_distanceEstimation));
        else if (SensorRightUnit_distanceEstimation < (15))
            _SteeringUnit.DriveStraightProportional(_SteeringUnit.STANDARD_DRIVE_SPEED,(SensorRightUnit_distanceEstimation-CALIBRATED_DISTANCE_WALL_LEFT));
        else
            _SteeringUnit.DriveStraight(_SteeringUnit.STANDARD_DRIVE_SPEED);
        
    }
    _SteeringUnit.Brake();
}

/**
 * \brief Checks if there is an Obstacle in front of the robot.
 *
 * Returns 1 if there is an obstacle ahead, or zero if there is not.
 * This function is not the inverse of isThereObstacleAhead since the threshold for determining
 * if there is a path and if there is an obstacle are different.
 */
int MazeGuider::isThereObstacleAhead() {
    SensorFrontUnit_distanceEstimation = _SensorFrontUnit.MeasureFilteredDistance(_SensorFrontUnit.MEASURY_UNIT_CM);
    if (SensorFrontUnit_distanceEstimation < (5)) return 1;
    else return 0;
}

/**
 * \brief Checks if there is a Path in front of the robot.
 *
 * Returns 1 if there is a path ahead, or zero if there is not.
 * This function is not the inverse of isThereObstacleAhead since the threshold for determining
 * if there is a path and if there is an obstacle are different.
 */
int MazeGuider::isTherePathToFront() {
    SensorFrontUnit_distanceEstimation = _SensorFrontUnit.MeasureFilteredDistance(_SensorFrontUnit.MEASURY_UNIT_CM);
    if (SensorFrontUnit_distanceEstimation > (MAZE_BLOCK_SIZE/2)) return 1;
    else return 0;
}

/**
 * \brief Checks if there is a Path to the left of the robot.
 *
 * Returns 1 if there is a path to left, or zero otherwise.
 */
int MazeGuider::isTherePathToLeft() {
    SensorLeftUnit_distanceEstimation = _SensorLeftUnit.MeasureFilteredDistance(_SensorLeftUnit.MEASURY_UNIT_CM);
    if (SensorLeftUnit_distanceEstimation > (2*MAZE_BLOCK_SIZE/3)) return 1;
    return 0;
}

/**
 * \brief Checks if there is a Path to the right of the robot.
 *
 * Returns 1 if there is a path to right, or zero otherwise.
 */
int MazeGuider::isTherePathToRight() {
    SensorRightUnit_distanceEstimation = _SensorRightUnit.MeasureFilteredDistance(_SensorRightUnit.MEASURY_UNIT_CM);
    if (SensorRightUnit_distanceEstimation > (2*MAZE_BLOCK_SIZE/3)) return 1;
    return 0;
}

/**
 * \brief Drives the robot back to the previous decision node.
 *
 * effect: the robot will turn 180 deegres and will driveToNextBlock() until isThereDecisionNode().
 */
void MazeGuider::returnPreviousDecision () {
    _SteeringUnit.TurnOwnAxis(180,_SteeringUnit.TURN_DIR_RIGHT);
    driveToNextBlock();

    switch ((*(*LastDecisionNode).getParent()).findChild(LastDecisionNode)){
        case 1:
            _SteeringUnit.TurnOwnAxis(180,_SteeringUnit.TURN_DIR_RIGHT);
            (*(*LastDecisionNode).getParent()).setDeadEnd((*LastDecisionNode).ALTERNATIVE_NODE_FRONT);
            break;
        case 2:
            _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_LEFT);
            (*(*LastDecisionNode).getParent()).setDeadEnd((*LastDecisionNode).ALTERNATIVE_NODE_LEFT);
            break;
        case 4:
            _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_RIGHT);
            (*(*LastDecisionNode).getParent()).setDeadEnd((*LastDecisionNode).ALTERNATIVE_NODE_RIGHT);
            break;
        default:
            //error
            break;
    }

    LastDecisionNode = (*LastDecisionNode).getParent();
    // Compensating the State value for deadends
    stateValue = getState();
    stateValue = stateValue - (4*((*LastDecisionNode).getDeadEnd((*LastDecisionNode).ALTERNATIVE_NODE_RIGHT))+1*((*LastDecisionNode).getDeadEnd((*LastDecisionNode).ALTERNATIVE_NODE_FRONT))+2*((*LastDecisionNode).getDeadEnd((*LastDecisionNode).ALTERNATIVE_NODE_LEFT)));
    executeStateDecision(stateValue);
}

/**
 * \brief Execute a movement based on the state given. Implement a State Machine.
 *
 * requires: currentState Check getState() function.
 * effect: The robot will make a move to the next block of the maze based on its state.
 */
byte MazeGuider::executeStateDecision(int currentState) {

    Serial.print("MazeGuider::executeStateDecision currentstate = ");
    Serial.println(currentState);

    switch (currentState) {
        case 0: // NO WALLS
            switch (random(3)) {
                case 0:
                    // FRONT
                    LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_FRONT);
                    stateReturn = 0;
                    break;
                case 1:
                    // LEFT
                    LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_LEFT);
                    _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_LEFT);
                    stateReturn = 3;
                    break;
                case 2:
                    // RIGHT
                    LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_RIGHT);
                    _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_RIGHT);
                    stateReturn = 1;
                    break;
                default:
                    break;
            }
            driveToNextBlock();
            break;
        case 1: // Wall on the front
            (*LastDecisionNode).setData(currentState);
            if(random(2))
            {
                // turn right
                LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_RIGHT);
                _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_RIGHT);
                    stateReturn = 1;
            } else {
                // turn left
                LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_LEFT);
                _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_LEFT);
                    stateReturn = 3;
            }
            driveToNextBlock();
            break;
        case 2: // Wall on the left
            (*LastDecisionNode).setData(currentState);
            if (random(2))
            {
                // turn right
                LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_RIGHT);
                _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_RIGHT);
                    stateReturn = 1;
            } else {
                // move forward
                LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_FRONT);
                    stateReturn = 0;
            }
            driveToNextBlock();
            break;
        case 3: // Wall on the front AND left
            // No decision to be stored
            _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_RIGHT);
            driveToNextBlock();
                    stateReturn = 1;
            break;
        case 4: // Wall on the right
            (*LastDecisionNode).setData(currentState);
            if (0) {
                // turn left
                LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_LEFT);
                _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_LEFT);
                stateReturn = 3;
            } else {
                // move forward
                LastDecisionNode = (*LastDecisionNode).addChild(0,(*LastDecisionNode).ALTERNATIVE_NODE_FRONT);
                stateReturn = 0;
            }
            driveToNextBlock();
            break;
        case 5: // Wall on the front AND right
            // no decision to be stored.
            _SteeringUnit.TurnOwnAxis(90,_SteeringUnit.TURN_DIR_LEFT);
            driveToNextBlock();
            stateReturn = 3;
            break;
        case 6: // Wall on the right AND left
            // no decision to be stored.
            calibrateDistanceToWalls();
            driveToNextBlock();
            stateReturn = 0;
            break;
        case 7: // Wall on the right AND left AND front (deadend)
            (*LastDecisionNode).setData(currentState);
            _SteeringUnit.Brake();
            returnPreviousDecision();
            stateReturn = 2;
            break;
        default:
            break;
    }

    return stateReturn;
}

/**
 * \brief Checks if there is a decision node.
 *
 * There is a decision node IF:
 * ((isThereObstacleAhead AND isTherePathToLeft AND isTherePathToRight) OR (!isTherePathToLeft AND isTherePathToRight AND !isThereObstacleAhead) OR (!isTherePathToRight AND isTherePathToLeft AND !isThereObstacleAhead))
 */
int MazeGuider::isThereDecisionNode() {
    if ((isThereObstacleAhead() && isTherePathToLeft() && isTherePathToRight()) || (!isTherePathToLeft() && !isTherePathToRight() && !isThereObstacleAhead()) || (!isTherePathToRight() && isTherePathToLeft() && !isThereObstacleAhead())) { return 1; }
    return 0;
}
