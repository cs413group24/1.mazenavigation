
#include "Arduino.h"
#include "Header.h"

#ifndef MazeMapper_h
#define MazeMapper_h

#define NORTH_DEFAULT 0
#define EAST_DEFAULT 1
#define SOUTH_DEFAULT 2
#define WEST_DEFAULT 3
#define IDLE_DEFAULT 4

#define ROWS 3
#define COLUMNS 3

class MazeMapper
{
    private: 
    //value array    
    byte values[ROWS][COLUMNS]; 
    byte robotRow;
    byte robotColumn;
    byte robotHeading;
    // vertical walls array
    boolean verticalWalls[ROWS][COLUMNS+1];
    // horizontal walls array
    boolean horizontalWalls[ROWS+1][COLUMNS];

    public:
        MazeMapper();
        void setRobotPosition(byte row, byte column);
        byte getRobotRow();
        byte getRobotColumn();
        byte getRobotHeading();
        void setRobotDirection(byte dir);
        void setTargetBlock(byte row, byte column);
        void updateWalls(int dir, byte state);
        void solveKnownMaze(MazeMapper m);
        void solveUnknownMaze();
        byte convertDirection(byte dir);
        void print();
        int NORTH,EAST,WEST,SOUTH,IDLE;



};

#endif
