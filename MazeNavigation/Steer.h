/*
  Steer.h - Library for controlling two motors rover
  created by IGOR CHAME (igorchame@poli.ufrj.br)
  on 04-Dec-2014
*/

#include "Arduino.h"
#include "Motor.h"
#include "UltrasonicSensor.h"

#ifndef Steer_h
#define Steer_h

#define TURN_DIR_LEFT_DEFAULT 1
#define TURN_DIR_RIGHT_DEFAULT 2

#define MS_TIME_FULL_TURN_OWN_AXIS_DEFAULT 10300
#define TURNING_SPEED_DEFAULT 15
#define STRAIGHT_DRIVE_BALANCING_RIGHTMOTOR_DEFAULT 0
#define STRAIGHT_DRIVE_BALANCING_LEFTMOTOR_DEFAULT 2
#define KP_DEFAULT 2
#define STANDARD_DRIVE_SPEED_DEFAULT 15
#define DIFERENCE_FRONT_SIDE_SENSOR 6  

class Steer
{
    public:
        Steer(Motor& ML, Motor& MR, UltrasonicSensor& ArgSensorFront, UltrasonicSensor& ArgSensorLeft, UltrasonicSensor& ArgSensorRight);
        void DriveStraight(int SpeedPercentage);
        void DriveStraightProportional(int SpeedPercentage, int Error);
        void TurnOwnAxis(int Angle, const int TurnDirection);
        void TurnOwnAxis90Degrees(const int TurnDirection);
        void Brake();
        int TURN_DIR_LEFT;
        int TURN_DIR_RIGHT;
        int STANDARD_DRIVE_SPEED;

    private:
        static Motor& _MotorLeft; // reference to Motor Object
        static Motor& _MotorRight; // reference to Motor Object
        static UltrasonicSensor& _SensorFrontUnit; // reference to Ultrassonic Sensor
        static UltrasonicSensor& _SensorRightUnit; // reference to Ultrassonic Sensor
        static UltrasonicSensor& _SensorLeftUnit; // reference to Ultrassonic Sensor
        static UltrasonicSensor& _RefiningSensor; // reference to Ultrassonic Sensor
        
        int GetDelayForTurn(int Angle);
        int MS_TIME_FULL_TURN_OWN_AXIS;
        int TURNING_SPEED;
        int STRAIGHT_DRIVE_BALANCING_RIGHTMOTOR;
        int STRAIGHT_DRIVE_BALANCING_LEFTMOTOR;
        int aux;
        float Kp;
        long frontDistance, leftDistance, rightDistance, newFrontDistance, newLeftDistance, newRightDistance, newRefiningDistance, refiningDistance;

};

#endif
