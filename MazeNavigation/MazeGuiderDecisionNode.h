/*
  MazeGuiderDecisionNode.h - Library for guiding a robot through a maze
  created by IGOR CHAME (igorchame@poli.ufrj.br)
  on Dec-2014
*/

#include "Arduino.h"

#ifndef MazeGuiderDecisionNode_h
#define MazeGuiderDecisionNode_h


using namespace std;

class MazeGuiderDecisionNode {

private:

    int _nodeState;
    MazeGuiderDecisionNode* parentDecisionNode;
    MazeGuiderDecisionNode* alternativeNodeLeft;
    byte _LeftDeadEnd;
    MazeGuiderDecisionNode* alternativeNodeRight;
    byte _RightDeadEnd;
    MazeGuiderDecisionNode* alternativeNodeFront;
    byte _FrontDeadEnd;

public:

    MazeGuiderDecisionNode(MazeGuiderDecisionNode* parentDecisionNodeArg, int nodeStateArg);
    ~MazeGuiderDecisionNode();

    int getData() const;
    void setData(int nodeStateArg);
    MazeGuiderDecisionNode* addChild(int nodeStateArg,int alternativeNode);
    void removeChild(int alternativeNode);
    void setDeadEnd(int alternativeNode);
    byte getDeadEnd(int alternativeNode);
    MazeGuiderDecisionNode* getChild(int alternativeNode) const;
    int findChild (MazeGuiderDecisionNode* childToFind);
    MazeGuiderDecisionNode* getParent() const;

    static const int ALTERNATIVE_NODE_FRONT = 1;
    static const int ALTERNATIVE_NODE_LEFT = 2;
    static const int ALTERNATIVE_NODE_RIGHT = 4;

};

#endif
