Technical Documentation                    {#mainpage}
============

# The goal of this project #

The goal of our project will be to build an autonomous robot that is able to navigate through a maze, in such a way that, knowing the starting point, it should be capable of getting out of it at the same time it is mapping the environment and keeping track of the previously explored paths.
The inspiration to this project comes from situations where the access to an indoor environment might represent a risk to humans. This robot, by the nature of its small dimensions and its range of sensors, could be able to easily reach hostile areas where a human wouldn’t be able to reach.
In a smaller scale, the maze problem represents a challenge of exploring a partially unknown environment without human interaction in order to achieve a goal, which in our case will be to reach the end of the maze.
In order to build this, we will be prototyping a small robot that is controlled with the Arduino prototyping board equipped with distance sensors, and a model maze will be made to test its performance.

## Aims & Objectives ##

This referred project has a set of objects to be accomplished; they are shown below with their deliverables:

* Main objective: develop an automatic robot that can navigate through a maze. The agent can either find a way out of it or a way to the centre through an autonomous process.
Deliverable: A robot and a program.
* Complimentary objective #1: develop a program and make the necessary modifications to enable the robot to map the maze. A computer-based program will also be developed so that the user can see the mapped maze. Deliverable: A program for the robot, and a computer-based program for map visualization.
* Complementary objective #2: develop a program and make the necessary modifications to enable the robot to find and retrieve a specific object inside the maze. Deliverable: A program and a modified robot.

Although we have three objectives listed, the actual implementation of the complimentary objectives will only be attempted in the case we achieve successful results from the previous objectives.


# Developer Team #

* Akitunde Adeyemo <kwb14203@uni.strath.ac.uk>
* David Pires <nwb13233@uni.strath.ac.uk>
* Igor Chame <igor.magrani-chame.2013@uni.strath.ac.uk>
* Mario Cecchi <macecchi@gmail.com>
